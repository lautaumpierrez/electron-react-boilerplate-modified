import React from 'react';
import {Link} from 'react-router-dom';

export default ()=>(
    <div>
        Welcome to the home
        <Link to="/otherpage">Ir a la otra pagina.</Link>
    </div>
);