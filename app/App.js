import React from 'react';
import { Provider } from 'react-redux';
import {HashRouter as Router, Switch, Route} from 'react-router-dom';
import { hot } from 'react-hot-loader/root';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk'
import {
  HomePage,
  OtherPage
} from './pages/';
import rootStore from './store/';
const store = createStore(rootStore,applyMiddleware(thunk));
const App = ()=>(
 <Provider store={store}>
    <Router>
        <Switch>
          <Route path="/" component={HomePage} exact={true}></Route>
          <Route path="/otherpage" component={OtherPage} exact={true}></Route>
        </Switch>
    </Router>
  </Provider>
);

export default hot(App);
